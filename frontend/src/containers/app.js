import React from 'react'
// import { Helmet } from "react-helmet";
import { BrowserRouter, browserHistory, Route, Redirect } from 'react-router-dom';

import * as steem from '@whaleshares/wlsjs';

import { Container } from 'semantic-ui-react'

import Account from '../containers/account'
import IndexLayout from '../components/layouts/index'
import FeedLayout from '../components/layouts/feed'
import ForumLayout from '../components/layouts/forum'
// import ForumCreateLayout from '../components/layouts/forum/create'
import ForumsLayout from '../components/layouts/forums'
import RepliesLayout from '../components/layouts/replies'
import ThreadLayout from '../components/layouts/thread'
import TopicLayout from '../components/layouts/topic'
import NewsPage from '../components/layouts/newspage'


import BreadcrumbMenu from '../components/global/breadcrumb'
import FooterMenu from '../components/global/footer'
import HeaderMenu from '../components/global/menu'
import GlobalNotice from '../components/global/notice'

import './app.css'
import '../../node_modules/noty/lib/noty.css'

steem.api.setOptions({ url: 'https://bitsharestalk.io/wss' });
steem.config.set('address_prefix','WLS');
steem.config.set('chain_id','de999ada2ff7ed3d3d580381f229b40b5a0261aec48eb830e540080817b72866');

/////////////////
// use withTracker:
// import withTracker from './withTracker'
// <Route exact path="/" component={withTracker(IndexLayout)} />

const App = () => (
    <BrowserRouter history={browserHistory}>
      <div className="AppContainer">
        {/*<Helmet>*/}
            {/*<title>BitsharesTalk</title>*/}
            {/*<meta name="description" content="Blockchain based decentralized forum software powered by the Whaleshares blockchain." />*/}
            {/*<meta name="viewport" content="width=device-width, initial-scale=1" />*/}
            {/*<meta itemprop="name" content="BitsharesTalk Forums" />*/}
            {/*<meta itemprop="description" content="Blockchain based decentralized forum software powered by the Whaleshares blockchain." />*/}
            {/*<meta itemprop="image" content="http://bitsharestalk.io/logo.png" />*/}
            {/*<meta name="twitter:card" content="summary" />*/}
            {/*<meta name="twitter:site" content="@bitsharestalk" />*/}
            {/*<meta name="twitter:title" content="BitsharesTalk Forums" />*/}
            {/*<meta name="twitter:description" content="Blockchain based decentralized forum software powered by the Whaleshares blockchain." />*/}
            {/*<meta name="twitter:creator" content="@officialfuzzy" />*/}
            {/*<meta name="twitter:image:src" content="https://bitsharestalk.io/logo.png" />*/}
            {/*<meta property="og:title" content="BitsharesTalk Forums" />*/}
            {/*/!*<meta property="og:type" content="article" />*!/*/}
            {/*<meta property="og:type" content="website" />*/}
            {/*<meta property="og:url" content="https://bitsharestalk.io/" />*/}
            {/*<meta property="og:image" content="http://bitsharestalk.io/logo.png" />*/}
            {/*<meta property="og:image:secure_url" content="https://bitsharestalk.io/logo.png" />*/}
            {/*<meta property="og:image:type" content="image/png" />*/}
            {/*<meta property="og:image:width" content="103" />*/}
            {/*<meta property="og:image:height" content="132" />*/}
            {/*<meta property="og:image:alt" content="BitsharesTalk logo" />*/}
            {/*<meta property="og:description" content="Blockchain based decentralized forum software powered by the Whaleshares blockchain." />*/}
            {/*<meta property="og:site_name" content="BitsharesTalk" />*/}
        {/*</Helmet>*/}
        <HeaderMenu />
        <BreadcrumbMenu />
        <GlobalNotice />
        <Container>
          <Route exact path="/newspage" component={NewsPage}/>

          {/* <Route exact path="/" component={IndexLayout} /> */}
          <Route exact path="/" render={(props) => <Redirect to="/forums"/>}/>
          <Route path="/@:username" component={Account} />
          {/*<Route path="/create/forum" component={ForumCreateLayout} />*/}
          <Route path="/feed" component={FeedLayout} />
          <Route path="/forums" component={ForumsLayout} />
          <Route path="/forums/:group" component={IndexLayout} />
          <Route path="/f/:id/:section?" component={ForumLayout} />
          <Route path="/forum/:id" render={(props) => <Redirect to={`/f/${props.match.params.id}`}/>}/>
          <Route path="/replies" component={RepliesLayout} />
          <Route path="/topic/:category" component={TopicLayout} />
          <Route path="/:category/@:author/:permlink/:action?" component={ThreadLayout} />
        </Container>
        <BreadcrumbMenu />
        <FooterMenu />
      </div>
    </BrowserRouter>
)

export default App
