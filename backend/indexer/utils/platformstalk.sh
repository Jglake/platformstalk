# ---------------------------------------------------------------------------
# btstalk + children
# ---------------------------------------------------------------------------

python3 reindex.py '{
  "_id": "platformstalk-announcements",
  "name": "Official Announcements",
  "group": "platformstalk",
  "group_order": 1,
  "forum_order": 1,
  "description": "Announcements from the official Platforms team",
  "tags": [],
  "accounts": ["platforms", "platformswitnes"]
}'

python3 reindex.py '{
  "_id": "platforms-general",
  "name": "General Discussion",
  "group": "platformstalk",
  "group_order": 1,
  "forum_order": 2,
  "description": "General discussions about Bitshares",
  "tags": ["platforms"]
}'

python3 reindex.py '{
  "_id": "platforms-projects",
  "name": "Specific Projects",
  "group": "platformstalk",
  "group_order": 1,
  "forum_order": 3,
  "description": "Updates for projects and efforts",
  "tags": ["platforms-project", "platforms", "project"]
}'

python3 reindex.py '{
  "_id": "platformstalk-members",
  "name": "Community Members",
  "group": "btstalk",
  "group_order": 1,
  "forum_order": 4,
  "description": "Community member chat, guides and project chatter.",
  "tags": ["platforms-community"]
}'

python3 reindex.py '{
  "_id": "platformstalk",
  "name": "Contests and Giveaways",
  "group": "platformstalk",
  "group_order": 1,
  "forum_order": 5,
  "description": "Contests and Giveaways",
  "tags": ["contests", "giveaways"]
}'
